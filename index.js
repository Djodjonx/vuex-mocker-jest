import _ from 'lodash'
import { modules } from '@/assets/scripts/store'

/* eslint-disable */
export default class VuexMocker {

  constructor() {
    this._internalModules = modules
    this._internalGetters = null
    this._internalActions = null
    this._internalMutations = null
    this._internalState = null
    this._vueMock = null
    this._id = Math.floor(Math.random() * 100000)
  }

  set(params) {
    return new Promise((resolve, reject) => {
      try {
        if (params.getters) {
          this._internalGetters = this.createGetters(params.getters)
        }
        if (params.actions) {
          this._internalActions = this.createActions(params.actions)
        }
        if (params.mutations) {
          this._internalMutations = this.createMutations(params.mutations)
        }
        if (params.state) {
          this._internalState = this.createStates(params.state)
        }

        this._vueMock = {
          state: this._internalState || {},
          getters: this._internalGetters || {},
          modules: this._internalModules || {},
          mutations: this._internalMutations || {},
          actions: this._internalActions || {},
        }
        resolve('done')
      } catch (e) {
        reject(e)
      }
    })

  }

  get() {
    return this._vueMock
  }

  getId() {
    return this._id
  }

  reset() {
    this._internalModules = modules
    this._internalGetters = null
    this._internalActions = null
    this._internalState = null
    this._vueMock = null
  }

  createGetters(getters) {
    const params = {}
    _.each(getters, (val, key) => {
      if (new RegExp('/').test(key)) {
        const splitKey = key.split('/')
        this._internalModules[splitKey[0]].getters[splitKey[1]] = () => val
      } else {
        params[key] = () => val
      }
    })
    return params
  }

  createActions(actions) {
    const params = {}
    _.each(actions, (val, key) => {
      if (new RegExp('/').test(key)) {
        const splitKey = key.split('/')
        this._internalModules[splitKey[0]].actions[splitKey[1]] = () => val
      } else {
        params[key] = () => val
      }
    })
    return params
  }
  createMutations(mutations) {
    const params = {}
    _.each(mutations, (val, key) => {
      if (new RegExp('/').test(key)) {
        const splitKey = key.split('/')
        this._internalModules[splitKey[0]].mutations[splitKey[1]] = () => val
      } else {
        params[key] = () => val
      }
    })
    return params
  }

  createStates(states) {
    const params = {}
    _.each(states, (val, key) => {
      if (Object.keys(this._internalModules).indexOf(key) !== -1) {
        _.each(val, (subVal, subKey) => {
          this._internalModules[key].state[subKey] = subVal
        })
      } else {
        params[key] = val
      }
    })
    return params
  }
}
