# General

This files was create for mocking Vuex from project for simulate it on component test in jest.

## Basic usage

Use this lib for overide **state, getters, actions and modules** from Vuex project to your tests.

## Geting start

1. Instanciate VuexMocker

```
import VuexMocker from '< absolute PATH >/test/helpers/vue/vuex'

const vuexMocker = new VuexMocker()
```

2. Create yours mocks
All mocks need to be Object.

__getters and actions__

```
// declare getters mock results

const getters = {
  'getBar': bar,
  'myModule/getSomething': baz,
  'myOtherModule/getFoo': false,
}

// declare actions mock results

const actions = {
  'setUser': jest.fn(() => '')
  'myModule/setFoo': jest.fn(() => ''),
  'myModule/setBar': jest.fn(() => ''),
}

```

All getters and actions from modules were binding to this modules directly, with using '< name module >/< (action - getter) >' like used on project.

__state__

Declare state and modules state like this:
```
// declare state mock results
state = {
  links: {
    foo: 'http://foo.com',
  }
  myModule: {
    bar: bar,
    baz: baz
  },
  myOtherModule: {
    bar: bar,
    foo: 'something'
  }
}
```

if module exist, your state was directly bind to it.


3. Bind to vue and use it

```
// initialize vueX before start all tests
beforeAll(async () => {
  await vuexMocker.set({ getters, state, actions })
  store = new Vuex.Store(vuexMocker.get())
})

beforeEach(() => {
wrapper = shallowMount(MyComponent, {
  store,
  localVue,
})
```
